import 'package:flutter/material.dart';

const primaryColor = Color(0xFF607185);
const backgroundColor = Color(0xFFF1F2F3);
const surfaceColor = Color(0xFF81A3A7);
const textPrimaryColor = Color(0xFF1C3146);
const textSecondaryColor = Color(0xFFC2D3DA);
const errorColor = Color(0xFFFF6A6A);
const navbarbackground = Color(0xFF272424);
