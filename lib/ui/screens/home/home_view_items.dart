import 'dart:ui';

import 'package:svuce_app/app/icons.dart';
import 'package:svuce_app/core/models/home_view_item.dart';
import 'package:svuce_app/ui/widgets/gridmenu_item.dart';

final List<HomeViewItem> homeViewItems = [
  HomeViewItem(
      title: "Home",
      activeIcon: homeIconFilled,
      inactiveIcon: homeIcon,
      scaleRatio: 25),
  HomeViewItem(
      title: "Feed",
      activeIcon: feedIconFilled,
      inactiveIcon: feedIcon,
      scaleRatio: 25),
];

final List<SpotlightItem> firstRowSpotlight = [
  SpotlightItem(
    icon: clubsIcon,
    name: "ADD",
    add: true,
    subname: 'Academic\nCourse',
    startColor: Color(0xffFA7D82),
    endColor: Color(0xffFFB295),
  ),
  SpotlightItem(
    icon: facultyIcon,
    subname: 'Over all\nevaluation',
    add: false,
    name: "VIEW",
    startColor: Color(0xff738AE6),
    endColor: Color(0xff5C5EDD),
  ),

  // SpotlightItem(
  //   icon: attendanceIcon,
  //   name: "Attendance",
  // ),
];

final List<SpotlightItem> secondRowSpotlight = [
  SpotlightItem(
    icon: hallOfFameIcon,
    add: false,
    name: "VIEW",
    subname: 'Detailed \nEvaluation',
    startColor: Color(0xffFE95B6),
    endColor: Color(0xffFF5287),
  ),
  SpotlightItem(
    icon: clubsIcon,
    add: false,
    name: "Explore Clubs",
    subname: '',
    startColor: Color(0xffFA7D82),
    endColor: Color(0xffFFB295),
  ),
  // SpotlightItem(
  //   icon: timeTableIcon,
  //   name: "Time table",
  //   startColor: Color(0xff6F72CA),
  //   endColor: Color(0xff1E1466),
  // ),
  // SpotlightItem(
  //   icon: campusIcon,
  //   name: "Campus",
  // ),
];
