import 'package:flutter/material.dart';
import 'package:svuce_app/app/colors.dart';
import 'package:svuce_app/app/default_view.dart';
import 'package:svuce_app/ui/screens/home/home_viewmodel.dart';
import 'home_view_items.dart';
import 'package:fl_chart/fl_chart.dart';
import 'indicators.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:percent_indicator/percent_indicator.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  bool status = true;
  int touchedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return ScreenBuilder<HomeViewModel>(
      viewModel: HomeViewModel(),
      onModelReady: (model) => model.getCurrentUserDetails(),
      builder: (context, uiHelpers, model) => WillPopScope(
        onWillPop: model.showExitSnackbar,
        child: Scaffold(
          body: ListView(
            padding: EdgeInsets.all(20.0),
            children: <Widget>[
              ListTile(
                contentPadding: EdgeInsets.all(0.0),
                title: Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: model.getGreeting(),
                        style: uiHelpers.title,
                      ),
                      TextSpan(
                        text: "\n" + model.currentUser.fullName,
                        style: uiHelpers.body.copyWith(
                            fontWeight: FontWeight.w600,
                            color: textSecondaryColor),
                      )
                    ],
                  ),
                ),
                trailing: ClipRRect(
                  borderRadius:
                      BorderRadius.circular(uiHelpers.scalingHelper.size(80)),
                  child: model.currentUser?.profileImg != null
                      ? Image.network(model.currentUser.profileImg,
                          fit: BoxFit.cover, width: 40, height: 40)
                      : SizedBox(),
                ),
              ),
              Text("\nEvaluate\n",
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20)),
              status == false
                  ? Container(
                      height: uiHelpers.blockSizeVertical * 20,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: surfaceColor.withAlpha(43),
                              spreadRadius: 2,
                              blurRadius: 5,
                              offset:
                                  Offset(0, 3), // changes position of shadow
                            ),
                          ],
                          color: backgroundColor,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Start Evaluate",
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              color: primaryColor,
                              size: 15,
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container(
                      height: uiHelpers.blockSizeVertical * 20,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xff738AE6).withAlpha(70),
                                offset: const Offset(1.1, 3.0),
                                blurRadius: 8.0), // changes position of shadow
                          ],
                          // border: Border.all(color: Color(0xff738AE6)),
                          color: backgroundColor,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            width: uiHelpers.blockSizeHorizontal * 30,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircularPercentIndicator(
                                  linearGradient: LinearGradient(
                                    colors: <Color>[
                                      Color(0xff738AE6),
                                      Color(0xff5C5EDD).withOpacity(0.3)
                                    ],
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                  ),
                                  radius: 120.0,

                                  lineWidth: 10.0,
                                  animation: true,
                                  // arcBackgroundColor: Colors.grey.withAlpha(40),
                                  percent: 0.7,
                                  // arcType: ArcType.FULL,
                                  rotateLinearGradient: false,
                                  center: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      new Text(
                                        "70.0%",
                                        style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20.0),
                                      ),
                                      new Text(
                                        "MCA",
                                        style: new TextStyle(
                                            // fontWeight: FontWeight.bold,
                                            fontSize: 15.0),
                                      ),
                                    ],
                                  ),
                                  // footer: new Text(
                                  //   "MCA",
                                  //   style: new TextStyle(
                                  //       fontWeight: FontWeight.bold,
                                  //       fontSize: 17.0),
                                  // ),
                                  circularStrokeCap: CircularStrokeCap.round,
                                  // progressColor: textPrimaryColor,
                                  backgroundColor: Colors.grey.withOpacity(0.2),
                                ),
                                Text('Current')
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 0),
                            child: Container(
                              height: uiHelpers.blockSizeVertical * 15,
                              width: 2,
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4.0)),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 30, top: 10),
                            child: Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                      child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Course',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 0, bottom: 5),
                                        child: Container(
                                          height: 4,
                                          width: 99,
                                          decoration: BoxDecoration(
                                            color: Color(0xffF56E98)
                                                .withOpacity(0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                width: uiHelpers
                                                        .blockSizeHorizontal *
                                                    10,
                                                height: 4,
                                                decoration: BoxDecoration(
                                                  gradient:
                                                      LinearGradient(colors: [
                                                    Color(0xffF56E98)
                                                        .withOpacity(0.1),
                                                    Color(0xffF56E98),
                                                  ]),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(4.0)),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      // Padding(
                                      //   padding: const EdgeInsets.only(top: 0),
                                      //   child: Container(
                                      //     height: 2,
                                      //     width: 60,
                                      //     decoration: BoxDecoration(
                                      //       color: Colors.grey.withOpacity(0.2),
                                      //       borderRadius: BorderRadius.all(
                                      //           Radius.circular(4.0)),
                                      //     ),
                                      //   ),
                                      // ),
                                      Text(
                                        'MCA',
                                        style: new TextStyle(
                                            // fontWeight: FontWeight.bold,
                                            ),
                                      )
                                    ],
                                  )),
                                  Container(
                                      child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Course',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 0, bottom: 5),
                                        child: Container(
                                          height: 4,
                                          width: 99,
                                          decoration: BoxDecoration(
                                            color: Color(0xffF56E98)
                                                .withOpacity(0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                width: uiHelpers
                                                        .blockSizeHorizontal *
                                                    10,
                                                height: 4,
                                                decoration: BoxDecoration(
                                                  gradient:
                                                      LinearGradient(colors: [
                                                    Color(0xffF56E98)
                                                        .withOpacity(0.1),
                                                    Color(0xffF56E98),
                                                  ]),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(4.0)),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Text(
                                        'MCA',
                                        style: new TextStyle(
                                            // fontWeight: FontWeight.bold,
                                            ),
                                      )
                                    ],
                                  )),
                                  // Padding(
                                  //   padding: const EdgeInsets.only(top: 0),
                                  //   child: Container(
                                  //     height: 2,
                                  //     width: 60,
                                  //     decoration: BoxDecoration(
                                  //       color: Colors.grey.withOpacity(0.2),
                                  //       borderRadius: BorderRadius.all(
                                  //           Radius.circular(4.0)),
                                  //     ),
                                  //   ),
                                  // ),
                                  Container(
                                      child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Course',
                                        style: new TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      // Padding(
                                      //   padding: const EdgeInsets.only(top: 0),
                                      //   child: Container(
                                      //     height: 2,
                                      //     width: 60,
                                      //     decoration: BoxDecoration(
                                      //       color: Colors.grey.withOpacity(0.2),
                                      //       borderRadius: BorderRadius.all(
                                      //           Radius.circular(4.0)),
                                      //     ),
                                      //   ),
                                      // ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 0, bottom: 5),
                                        child: Container(
                                          height: 4,
                                          width: 99,
                                          decoration: BoxDecoration(
                                            color: Color(0xffF56E98)
                                                .withOpacity(0.2),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(4.0)),
                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Container(
                                                width: uiHelpers
                                                        .blockSizeHorizontal *
                                                    10,
                                                height: 4,
                                                decoration: BoxDecoration(
                                                  gradient:
                                                      LinearGradient(colors: [
                                                    Color(0xffF56E98)
                                                        .withOpacity(0.1),
                                                    Color(0xffF56E98),
                                                  ]),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(4.0)),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Text(
                                        'MCA',
                                        style: new TextStyle(
                                            // fontWeight: FontWeight.bold,
                                            ),
                                      )
                                    ],
                                  )),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
              Text(
                "\nIn the spotlight\n",
                style: uiHelpers.title,
              ),
              Wrap(
                direction: Axis.horizontal,
                children: firstRowSpotlight.map((eachSpotLight) {
                  if (eachSpotLight.name == "ADD") {
                    return GestureDetector(
                      onTap: () => model.add_course(),
                      child: eachSpotLight,
                    );
                  } else {
                    return eachSpotLight;
                  }
                }).toList(),
                alignment: WrapAlignment.spaceBetween,
              ),
              uiHelpers.verticalSpaceLow,
              Wrap(
                children: secondRowSpotlight,
                alignment: WrapAlignment.spaceBetween,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
