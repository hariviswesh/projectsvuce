import 'package:flutter/material.dart';
import 'package:svuce_app/app/colors.dart';
import 'package:svuce_app/app/default_view.dart';
import 'package:svuce_app/core/utils/ui_helpers.dart';

class SpotlightItem extends StatelessWidget {
  final IconData icon;
  final String name;
  final String subname;
  final bool add;

  final Color startColor;
  final Color endColor;

  const SpotlightItem(
      {Key key,
      @required this.icon,
      @required this.add,
      @required this.name,
      @required this.subname,
      @required this.startColor,
      @required this.endColor})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);

    var screenWidth = mediaQuery.size.width;
    var screenHeight = mediaQuery.size.height;
    return Wrap(
      direction: Axis.vertical,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: <Widget>[
        Container(
          width: screenWidth / 100 * 42,
          height: screenHeight / 100 * 16,
          // alignment: Alignment.center,
          margin: EdgeInsets.only(bottom: 10.0),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[startColor, endColor.withOpacity(0.3)],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: endColor.withOpacity(0.6),
                    offset: const Offset(1.1, 4.0),
                    blurRadius: 8.0),
              ],
              borderRadius: BorderRadius.circular(10),
              // borderRadius: const BorderRadius.only(
              //   bottomRight: Radius.circular(8.0),
              //   bottomLeft: Radius.circular(8.0),
              //   topLeft: Radius.circular(8.0),
              //   topRight: Radius.circular(54.0),
              // ),
              color: backgroundColor),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    letterSpacing: 0.2,
                  ),
                ),
                SizedBox(
                  height: 3,
                ),
                Text(
                  subname,
                  style: TextStyle(
                    color: Colors.white,
                    // fontWeight: FontWeight.bold,
                    fontSize: 18,
                    letterSpacing: 0.3,
                  ),
                ),
                add == true
                    ? Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.add,
                            size: 21,
                            color: Colors.white,
                          ),
                        ],
                      )
                    : SizedBox()
              ],
            ),
          ),
          // child: Icon(
          //   icon,
          //   color: primaryColor,
          // ),
          padding: EdgeInsets.all(20.0),
        ),
        // Text(
        //   name,
        //   style: Theme.of(context)
        //       .textTheme
        //       .caption
        //       .copyWith(color: textPrimaryColor),
        // )
      ],
    );
  }
}
